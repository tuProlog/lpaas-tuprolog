package andreamuccioli.smartbathroomapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import alice.tuprolog.Term;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

//  List containing the current list of solutions from the servers. Both the list and the counter are
//  accessed concurrently.
    private List<String> serverSolutionList = new ArrayList<>();
    private AtomicInteger taskCounter = new AtomicInteger(3);
    private final Object listLock = new Object();

    //servers url, selected in the settings page
    private String currentToiletURL = "http://";
    private String currentToothbrushURL = "http://";
    private String currentWearableURL = "http://";

//    icons
    public ImageView toiletImage;
    public ImageView toothImage;
    public ImageView wearableImage;

//    listview and related objects needed to manage the inference engine solutions
    public ListView listViewO;
    public ListView listViewG;
    public ArrayAdapter<String> adapterOrange;
    public ArrayAdapter<String> adapterGreen;
    private List<String> warningListOrange = new ArrayList<>();
    private List<String> warningListGreen = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //contact us button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                SendEmailFragment sendMailDialogFrag = new SendEmailFragment();
                sendMailDialogFrag.show(fm, "fragment_send_email");
            }
        });

        //set up the parameters
        toiletImage = (ImageView) findViewById(R.id.toiletImageView);
        toothImage = (ImageView) findViewById(R.id.toothImageView);
        wearableImage = (ImageView) findViewById(R.id.wearableImageView);

        listViewO = (ListView) findViewById(R.id.orangeListView);
        adapterOrange = new ArrayAdapter<>(this, R.layout.centeredroworange, R.id.textView5, warningListOrange);
        listViewO.setAdapter(adapterOrange);

        listViewG = (ListView) findViewById(R.id.greenListView);
        adapterGreen = new ArrayAdapter<>(this, R.layout.centeredrowgreen, R.id.textView5, warningListGreen);
        listViewG.setAdapter(adapterGreen);

        //Button to check the status of the 3 smartbathroom servers
        Button checkStatusButton = (Button) findViewById(R.id.checkStatusButton);
        checkStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toiletImage.setBackgroundResource(R.drawable.circleobitmap);
                toothImage.setBackgroundResource(R.drawable.circleobitmap);
                wearableImage.setBackgroundResource(R.drawable.circleobitmap);

                new CheckStatusTask().execute(SmartBathroomServer.TOILET);
                new CheckStatusTask().execute(SmartBathroomServer.TOOTHBRUSH);
                new CheckStatusTask().execute(SmartBathroomServer.WEARABLE);
            }
        });

        //Button to load the data from the servers and update the UI
        Button warningsButton = (Button) findViewById(R.id.getWarningsButton);
        warningsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                taskCounter.set(3);
                serverSolutionList = new ArrayList<>();

                warningListOrange.clear();
                warningListGreen.clear();
                adapterGreen.notifyDataSetChanged();
                adapterOrange.notifyDataSetChanged();

                toiletImage.setBackgroundResource(R.drawable.circleobitmap);
                toothImage.setBackgroundResource(R.drawable.circleobitmap);
                wearableImage.setBackgroundResource(R.drawable.circleobitmap);

                new DownloadData().execute(SmartBathroomServer.TOILET);
                new DownloadData().execute(SmartBathroomServer.TOOTHBRUSH);
                new DownloadData().execute(SmartBathroomServer.WEARABLE);
            }
        });
        //update URL data from settings
        updateUrlPrefs();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            //Settings
            case R.id.action_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                return true;
            //About
            case R.id.action_about:
                FragmentManager fm = getSupportFragmentManager();
                AboutDialogFragment aboutDialogFrag = new AboutDialogFragment();
                aboutDialogFrag.show(fm, "fragment_about");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Update url data from settings
    public void updateUrlPrefs() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        currentToiletURL = sharedPreferences.getString("pref_toiletURL", "http://");
        currentToothbrushURL = sharedPreferences.getString("pref_toothbrushURL", "http://");
        currentWearableURL = sharedPreferences.getString("pref_wearableURL", "http://");
    }

    //Update background color of the icons based on the state of the server
    private void updateServerStatusColor(SmartBathroomServer server, boolean alive) {
        switch (server) {
            case TOILET:
                if (alive)
                    toiletImage.setBackgroundResource(R.drawable.circlegbitmap);
                else
                    toiletImage.setBackgroundResource(R.drawable.circleobitmap);
                break;
            case TOOTHBRUSH:
                if (alive)
                    toothImage.setBackgroundResource(R.drawable.circlegbitmap);
                else
                    toothImage.setBackgroundResource(R.drawable.circleobitmap);
                break;
            case WEARABLE:
                if (alive)
                    wearableImage.setBackgroundResource(R.drawable.circlegbitmap);
                else
                    wearableImage.setBackgroundResource(R.drawable.circleobitmap);
                break;
            default:
                break;
        }
    }

    //update height of the given listview based on the number of entries contained
    private void updateListViewHeight(ListView lv, ArrayAdapter<String> adapter) {
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, lv);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = lv.getLayoutParams();
        params.height = totalHeight + (lv.getDividerHeight() * (adapter.getCount() - 1));
        lv.setLayoutParams(params);
        lv.requestLayout();
    }


//AsyncTask to check the status of the servers. Send a request to the /main page and check if the response is "ok"
    class CheckStatusTask extends AsyncTask<SmartBathroomServer, Void, Boolean> {
        private Exception e = null;
        private SmartBathroomServer server = null;

        @Override
        protected Boolean doInBackground(SmartBathroomServer... destinations) {
            try {
                Request req;
                switch (destinations[0]) {
                    case TOILET:
                        req = getRequest(currentToiletURL);
                        server = SmartBathroomServer.TOILET;
                        break;
                    case TOOTHBRUSH:
                        req = getRequest(currentToothbrushURL);
                        server = SmartBathroomServer.TOOTHBRUSH;
                        break;
                    case WEARABLE:
                        req = getRequest(currentWearableURL);
                        server = SmartBathroomServer.WEARABLE;
                        break;
                    default:
                        return false;
                }
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(3, TimeUnit.SECONDS)
                        .writeTimeout(3, TimeUnit.SECONDS)
                        .readTimeout(3, TimeUnit.SECONDS)
                        .build();
                Response response = client.newCall(req).execute();
                return response.code() == 200;
            } catch (IOException e) {
                this.e = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(Boolean alive) {
            if (this.e != null) {
                e.printStackTrace();
                switch (server) {
                    case TOILET:
                        toiletImage.setBackgroundResource(R.drawable.circleobitmap);
                        break;
                    case TOOTHBRUSH:
                        toothImage.setBackgroundResource(R.drawable.circleobitmap);
                        break;
                    case WEARABLE:
                        wearableImage.setBackgroundResource(R.drawable.circleobitmap);
                        break;
                    default:
                        break;
                }
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
            else {
                updateServerStatusColor(server, alive);
            }

        }
        //generate standard request from given url
        private Request getRequest(String url) {
            return new Request.Builder().url(url).get().build();
        }

    }

    //AsyncTask to download the data from the servers, each async task sends 3 requests: the first one
    //to check if the server is alive, the second and the third one to solve the two goals
    //available for each server.
    class DownloadData extends AsyncTask<SmartBathroomServer, Void, List<String>> {
        private Exception e = null;
        private SmartBathroomServer server = null;
        private Boolean alive = false;

        @Override
        protected List<String> doInBackground(SmartBathroomServer... destinations) {
            Request checkReq;
            Request req1;
            Request req2;
            String urlData;
            switch (destinations[0]) {
                case TOILET:
                    checkReq = getCheckRequest(currentToiletURL);
                    urlData = (currentToiletURL.endsWith("/")) ? (currentToiletURL + "solutions") : (currentToiletURL + "/solutions");
                    req1 = getDataRequest(urlData, "disease(X).");
                    req2 = getDataRequest(urlData, "symptom(X).");
                    server = SmartBathroomServer.TOILET;
                    break;
                case TOOTHBRUSH:
                    checkReq = getCheckRequest(currentToothbrushURL);
                    urlData = (currentToothbrushURL.endsWith("/")) ? (currentToothbrushURL + "solutions") : (currentToothbrushURL + "/solutions");
                    req1 = getDataRequest(urlData, "warning(X).");
                    req2 = getDataRequest(urlData, "symptom(X).");
                    server = SmartBathroomServer.TOOTHBRUSH;
                    break;
                case WEARABLE:
                    checkReq = getCheckRequest(currentWearableURL);
                    urlData = (currentWearableURL.endsWith("/")) ? (currentWearableURL + "solutions") : (currentWearableURL + "/solutions");
                    req1 = getDataRequest(urlData, "disease(X).");
                    req2 = getDataRequest(urlData, "symptom(X).");
                    server = SmartBathroomServer.WEARABLE;
                    break;
                default:
                    return null;
            }
            try {
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(3, TimeUnit.SECONDS)
                        .writeTimeout(3, TimeUnit.SECONDS)
                        .readTimeout(3, TimeUnit.SECONDS)
                        .build();
                Response responseCheck = client.newCall(checkReq).execute();
                alive = responseCheck.code() == 200;

                if (alive) {

                    Response response1 = client.newCall(req1).execute();
                    Response response2 = client.newCall(req2).execute();

                    Gson gson = new Gson();
                    List<String> results1 = gson.fromJson(response1.body().string(), List.class);
                    List<String> results2 = gson.fromJson(response2.body().string(), List.class);
                    results1.addAll(results2);

                    return results1;
                }
                else
                    return null;


            } catch (IOException e) {
                this.e = e;
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<String> results) {
            synchronized (listLock) {
                if (!alive) {
                    updateServerStatusColor(server, alive);
                    taskCounter.decrementAndGet();
                    return;
                }
                if (e != null) {
                    e.printStackTrace();
                    switch (server) {
                        case TOILET:
                            toiletImage.setBackgroundResource(R.drawable.circleobitmap);
                            break;
                        case TOOTHBRUSH:
                            toothImage.setBackgroundResource(R.drawable.circleobitmap);
                            break;
                        case WEARABLE:
                            wearableImage.setBackgroundResource(R.drawable.circleobitmap);
                            break;
                        default:
                            break;
                    }
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    taskCounter.decrementAndGet();
                    return;
                }

                updateServerStatusColor(server, true);
                serverSolutionList.addAll(results);

                //if this is the last task to complete execution: it gets the solution from the inference engine
                if (taskCounter.decrementAndGet() == 0) {

                    //updates knowledge base: retracts all old terms and asserts all new ones
                    InferenceEngine.getInstance().retractAll();
                    for (String infoString : serverSolutionList) {
                        if (InferenceEngine.isSuccess(infoString)) {
                            InferenceEngine.getInstance().assertTerm(InferenceEngine.getSolution(infoString));
                        }
                    }

                    //obtains warning lists
                    List<String> warnings1 = null;
                    List<String> warnings2 = null;
                    Term goal1 = Term.createTerm("warning1(X)");
                    Term goal2 = Term.createTerm("warning2(X)");
                    warnings1 = InferenceEngine.getInstance().solveAllPrettyString(goal1);
                    warnings2 = InferenceEngine.getInstance().solveAllPrettyString(goal2);

                    //update UI
                    updateUI(warnings1, warnings2);

                    //reset counter
                    taskCounter.set(3);

                }

            }


        }

        private void updateUI(List<String> warnings1, List<String> warnings2) {
            for (String warning : warnings2) {
                warningListOrange.add(warning);
            }

            for (String warning : warnings1) {
                warningListGreen.add(warning);
            }
            adapterOrange.notifyDataSetChanged();
            adapterGreen.notifyDataSetChanged();
            updateListViewHeight(listViewO, adapterOrange);
            updateListViewHeight(listViewG, adapterGreen);

        }

        private Request getCheckRequest(String url) {
            return new Request.Builder().url(url).get().build();
        }

        private Request getDataRequest(String url, String goal) {
            RequestBody body = RequestBody.create(MediaType.parse("text/plain; charset=utf-8"), goal);
            return new Request.Builder().url(url).post(body).build();
        }

    }
}
