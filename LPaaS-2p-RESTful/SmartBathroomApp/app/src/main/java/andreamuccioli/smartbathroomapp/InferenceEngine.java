package andreamuccioli.smartbathroomapp;

import java.util.ArrayList;
import java.util.List;

import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.NoMoreSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;

//wrapper of the tuProlog engine
class InferenceEngine {

    private volatile static InferenceEngine instance = null;
    private List<Term> clientAssertions;
    private Prolog engine;

    private InferenceEngine() {
        clientAssertions = new ArrayList<>();
        engine = new Prolog();
        try {
            Theory t = new Theory(getTheory());
            engine.setTheory(t);
        } catch (InvalidTheoryException e) {
            e.printStackTrace();
        }
    }


    static InferenceEngine getInstance() {
        if (instance == null) {
            synchronized (InferenceEngine.class) {
                if (instance == null)
                    instance = new InferenceEngine();
            }
        }
            return instance;
    }

    private void resetClientAssertions() {
        clientAssertions = new ArrayList<>();
    }

    void retractAll() {
        synchronized (InferenceEngine.class) {
            for (Term assertion : clientAssertions) {
                Term retract = Term.createTerm("retract( ("+assertion.toString()+") )");
                engine.solve(retract);
            }
            resetClientAssertions();
        }
    }

    void assertTerm(Term sol) {
        synchronized (InferenceEngine.class) {
            clientAssertions.add(sol);
            Term assertion = Term.createTerm("assert( (" + sol.toString() + ") )");
            engine.solve(assertion);
        }
    }

    //returns all the solutions for the goal as a list of SolveInfo
    private List<SolveInfo> solveAll(Term toSolve) throws NoMoreSolutionException {
        synchronized (InferenceEngine.class) {
            List<SolveInfo> list = new ArrayList<>();
            list.add(engine.solve(toSolve));
            while (engine.hasOpenAlternatives()) {
                list.add(engine.solveNext());
            }
            return list;
        }
    }

    //returns all the solutions for the goal as a list of String
    private List<String> solveAllString(Term toSolve) {
        synchronized (InferenceEngine.class) {
            try {
                List<SolveInfo> infos = solveAll(toSolve);
                List<String> toReturn = new ArrayList<>();
                for (SolveInfo info : infos) {
                    if (info.isSuccess()) {
                        String value = info.getVarValue("X").toString();
                        toReturn.add(value);
                    }
                }
                return toReturn;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
//  returns all the solutions for the goal as a list of well formatted String ( '_' replaced with
//  whitespaces and starting character capitalized)
    List<String> solveAllPrettyString(Term toSolve) {
        synchronized (InferenceEngine.class) {
            List<String> results = solveAllString(toSolve);
            List<String> prettyResults = new ArrayList<>();
            for (String res : results) {
                String prettyRes = res.replace('_', ' ');
                prettyResults.add(prettyRes.substring(0, 1).toUpperCase() + prettyRes.substring(1));
            }
            return prettyResults;

        }
    }

//  determines success of the solution from its string form
    static boolean isSuccess(String solveinfoString) {
        return solveinfoString.startsWith("yes.");
    }
//  gets the value of the solution from its string form
    static Term getSolution(String solveinfoString) {
        String[] lines = solveinfoString.split("\n");
        String[] linesSol = lines[1].split(" / ");

        return Term.createTerm(linesSol[1]);
    }

    private String getTheory() {
        String sb = "warning1(limit_sodium_intake) :- high_urine_sodium, high_blood_pressure.\n" +
                "warning1(drink_more_water) :- dehydration.\n" +
                "warning1(high_blood_pressure) :- high_blood_pressure.\n" +
                "warning1(low_blood_pressure) :- low_blood_pressure.\n" +
                "warning1(brush_your_teeth) :- brush_your_teeth.\n" +
                "warning1(toothbrush_battery_low) :- battery_low.\n" +
                "warning2(oral_infection) :- fever, infection.\n" +
                "warning2(urinary_tract_infection) :- fever, urine_wbc.\n" +
                "warning2(screening_colon) :- occult_blood.\n" +
                "warning2(possible_prediabetes) :- prediabetes.\n" +
                "warning2(possible_diabetes) :- diabetes.\n";

        return sb;
    }
}
