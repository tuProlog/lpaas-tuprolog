package andreamuccioli.smartbathroomapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;



public class SendEmailFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_contact_title)
                .setMessage(getResources().getString(R.string.dialog_contact_message) + getResources().getString(R.string.dialog_contact_email))
                .setPositiveButton(R.string.dialog_contact_positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(Intent.ACTION_SENDTO);
                        String uriText = "mailto:" + Uri.encode(getResources().getString(R.string.dialog_contact_email));
                        Uri uri = Uri.parse(uriText);
                        i.setData(uri);
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    }
                })
                .setNegativeButton(R.string.dialog_contact_negative, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }

}
