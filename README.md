# README #

LPaaS is an an effective enabling technology for intelligent IoT. It is a
logic-based, service-oriented approach for distributed situated intelligence, 
conceived and designed as the natural evolution of LP in nowadays pervasive computing systems. 
Its purpose is to enable situated reasoning via explicit definition of the spatial- temporal structure 
of the environment where situated entities act and interact.

LPaaS in tuProlog consists in the LPaaS implementation built on top of the [tuProlog system](https://bitbucket.org/tuprologteam/tuprolog),
that makes it possible to build distributed intelligent IoT environment 
distributing reasoning and inference capabilities amongst the components they have, 
and let them balance the computational requirements to best suit the deployment scenario 
at hand�for instance, embedding LPaaS in more powerful components and letting 
ask their services by need.

The main reasearch articles introducing LPaaS can be found here:

* [Logic Programming as a Service](http://apice.unibo.it/xwiki/bin/view/Publications/LpaasTplp2017)
* [Logic Programming as a Service (LPaaS): Intelligence for the IoT](http://apice.unibo.it/xwiki/bin/view/Publications/LpaasIcnsc2017)
* [Towards Logic Programming as a Service: Experiments in tuProlog](http://apice.unibo.it/xwiki/bin/view/Publications/LpaasWoa2016)

### What is this repository for? ###

The repository contains prototype implementations built on top of the tuProlog system, 
which provides the required interoperability and customisation.

Currently, two different prototype implementations are available -namely as a web service 
and as an agent in a multi-agent system (MAS).

### How do I get set up? ###

* Setup instruction for LPaaS as a RESTful Web Service can be downloaded [here](https://bitbucket.org/tuProlog/lpaas-tuprolog/downloads/InstallationGuide-LPaaS-as-a-RESTfulWS.pdf)
* Setup instruction for LPaaS in a MAS can be downloaded [here](https://bitbucket.org/tuProlog/lpaas-tuprolog/downloads/InstallationGuide-LPaaS-JADEAgent.pdf)