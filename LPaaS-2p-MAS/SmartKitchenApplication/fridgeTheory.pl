fridgeGroceryList(Type, Threshold, UnitOfMesure, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(fridgeData(D), fridgeData(D), ListData),
	analyzeFridgeDataList(Type, Threshold, UnitOfMesure, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
fridgeGroceryList(Threshold, UnitOfMesure, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(fridgeData(D), fridgeData(D), ListData),
	analyzeFridgeDataList(Threshold, UnitOfMesure, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
fridgeGroceryList(Threshold, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(fridgeData(D), fridgeData(D), ListData),
	analyzeFridgeDataList(Threshold, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
analyzeFridgeDataList(Type, Threshold, UnitOfMesure, []).
analyzeFridgeDataList(Type, Threshold, UnitOfMesure, [fridgeData(L)|T]) :-
	processEntry(Type, Threshold, UnitOfMesure, L), 
	analyzeFridgeDataList(Type, Threshold, UnitOfMesure, T).
	
analyzeFridgeDataList(Threshold, UnitOfMesure, []).
analyzeFridgeDataList(Threshold, UnitOfMesure, [fridgeData(L)|T]) :-
	processEntry(Threshold, UnitOfMesure, L), 
	analyzeFridgeDataList(Threshold, UnitOfMesure, T).
	
analyzeFridgeDataList(Threshold, []).
analyzeFridgeDataList(Threshold, [fridgeData(L)|T]) :-
	processEntry(Threshold, L), 
	analyzeFridgeDataList(Threshold, T).
	
processEntry(Type, Threshold, UnitOfMesure, []).
processEntry(Type, Threshold, UnitOfMesure, [fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Type, Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Type, Threshold, UnitOfMesure, T).
	
processEntry(Threshold, UnitOfMesure, []).
processEntry(Threshold, UnitOfMesure, [fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Threshold, UnitOfMesure, T).

processEntry(Threshold, []).
processEntry(Threshold, [fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Threshold, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Threshold, T).

verify(Type, Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	Type == ItemType,
	UnitOfMesure == ItemUnitOfMesure,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Type, Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

verify(Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	UnitOfMesure == ItemUnitOfMesure,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Threshold, UnitOfMesure, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

verify(Threshold, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Threshold, fridgeSupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

fridgeModelInfo(Result) :-
	fridgeModel(A, B),
	Result = fridgeModel(A ,B).
	
getFridgeTemperature(T, S) :-
	getTime(O),
	fridgeTemperature(A, B, timestamp(O)),
	T=A,
	S=B,
	findall(fridgeTemperature(A, B, C), fridgeTemperature(A, B, C), L),
	retractDataList(L),
	assert(fridgeTemperature(A, B, timestamp(O))), !.
	
fridgeCoolingSystemStatus(S) :-
	getTime(O),
	fridgeCooling(A, timestamp(O)),
	S=A,
	findall(fridgeCooling(A, B), fridgeCooling(A, B), L),
	retractDataList(L),
	assert(fridgeCooling(A, timestamp(O))), !.
	
fridgeAntiIceSystemStatus(S) :-
	getTime(O),
	fridgeAntiIce(A, timestamp(O)),
	S=A,
	findall(fridgeAntiIce(A, B), fridgeAntiIce(A, B), L),
	retractDataList(L),
	assert(fridgeAntiIce(A, timestamp(O))), !.
	
fridgeStatus(S) :-
	getTime(O),
	fridgeStatus(A, timestamp(O)),
	S=A,
	findall(fridgeStatus(A, B), fridgeStatus(A, B), L),
	retractDataList(L),
	assert(fridgeStatus(A, timestamp(O))), !.

retractDataList([]).
retractDataList([H|T]) :- 
	retract(H), retractDataList(T).
	
naiverev([],[]). 
naiverev([H|T],R) :-
	naiverev(T, RevT),  
	append(RevT,[H],R).
	
getTime(M) :-
	findall(fridgeLogicTime(T), fridgeLogicTime(T), L),
	naiverev(L, [fridgeLogicTime(U)|T]),
	M = U,
	retractDataList(L),
	assert(fridgeLogicTime(M)), !.

%tickTime :-
%	getTime(M),
%	N is M+1,
%	assert(fridgeLogicTime(N)), !.
	
%simulated Data -----

fridgeLogicTime(0).

fridgeData([fridgeSupply(mela, frutta, 'brand-z', 2, unit, timestamp(0)),
	fridgeSupply(pera, frutta, 'brand-y', 3, unit, timestamp(0)),
	fridgeSupply(kiwi, frutta, 'brand-x', 1, unit, timestamp(0)),
	fridgeSupply(banana, frutta, 'brand-x', 3, unit, timestamp(0)),
	fridgeSupply(arancia, frutta, 'brand-y', 10, unit, timestamp(0)),
	fridgeSupply(caco, frutta, 'brand-x', 4, unit, timestamp(0)),
	fridgeSupply(anguria, frutta, 'brand-y', 4, unit, timestamp(0)),
	fridgeSupply(pesca, frutta, 'brand-x', 0, unit, timestamp(0)),
	fridgeSupply(pompelmo, frutta, 'brand-x', 2, unit, timestamp(0))]).
	
fridgeData([fridgeSupply(insalata, verdura, 'brand-z', 2, unit, timestamp(0)),
	fridgeSupply(pomodori, verdura, 'brand-y', 3, unit, timestamp(0))]).

fridgeModel(samsung, xvcd567yuhGo).

fridgeTemperature(-5, gradi_celsius, timestamp(0)).

fridgeCooling(on, timestamp(0)).

fridgeAntiIce(on, timestamp(0)).

fridgeStatus(on, timestamp(0)).
			
