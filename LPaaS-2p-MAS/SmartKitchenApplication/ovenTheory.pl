getRecentCookedMeals(Rlist) :-
	findall(recentMeal(X), recentMeal(X), Rl),
	retractDataList(Rl),
	findall(ovenData(D), ovenData(D), M),
	findRecentMeals(M),
	findall(recentMeal(X), recentMeal(X), Rlist),
	retractDataList(Rlist), !.
	
findRecentMeals([]).
findRecentMeals([ovenData(L)|T]) :-
	checkMeal(L),
	findRecentMeals(T).

checkMeal([]).
checkMeal([cooked(V, timestamp(M))|T]) :-
	getTime(B),
	B==M,
	assert(recentMeal(V)),
	checkMeal(T).
checkMeal([cooked(V, timestamp(M))|T]) :-
	checkMeal(T).	

getOvenTemperature(T, S) :-
	getTime(O),
	ovenTemperature(A, B, timestamp(O)),
	T=A,
	S=B,
	findall(ovenTemperature(A, B, C), ovenTemperature(A, B, C), L),
	retractDataList(L),
	assert(ovenTemperature(A, B, timestamp(O))), !.
	
ovenCoolingSystemStatus(S) :-
	getTime(O),
	ovenCooling(A, timestamp(O)),
	S=A,
	findall(ovenCooling(A, B), ovenCooling(A, B), L),
	retractDataList(L),
	assert(ovenCooling(A, timestamp(O))), !.
	
ovenStatus(S) :-
	getTime(O),
	ovenStatus(A, timestamp(O)),
	S=A,
	findall(ovenStatus(A, B), ovenStatus(A, B), L),
	retractDataList(L),
	assert(ovenStatus(A, timestamp(O))), !.
	
ovenModelInfo(Result) :-
	ovenModel(A, B),
	Result = ovenModel(A, B).
	
retractDataList([]).
retractDataList([H|T]) :- 
	retract(H), retractDataList(T).
	
naiverev([],[]). 
naiverev([H|T],R) :-
	naiverev(T, RevT),  
	append(RevT,[H],R).

getTime(M) :-
	findall(ovenLogicTime(T), ovenLogicTime(T), L),
	naiverev(L, [ovenLogicTime(U)|T]),
	M = U,
	retractDataList(L),
	assert(ovenLogicTime(M)), !.

%tickTime :-
%	getTime(M),
%	N is M+1,
%	assert(ovenLogicTime(N)), !.
	
%simulated Data -----

ovenLogicTime(0).

ovenData([cooked([pollo, patate, pomodorini], timestamp(0)),
	cooked([crostata, mele, pere], timestamp(0)),
	cooked([arrosto, patate], timestamp(0)),
	cooked([pizza, margherita, pomodoro, mozzarella, origano], timestamp(0)),
	cooked([pizza, cotto, carciofini], timestamp(0))]).
	
ovenTemperature(20, gradi_celsius, timestamp(0)).

ovenCooling(off, timestamp(0)).

ovenStatus(off, timestamp(0)).
	
ovenModel(philips, vbghn6678m).