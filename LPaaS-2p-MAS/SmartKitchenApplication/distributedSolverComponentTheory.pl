groceryList(Type, Threshold, UnitOfMesure, Supply) :-
	distributedSolve(remote(fridgeGroceryList(Type, Threshold, UnitOfMesure, L)), remote(pantryGroceryList(Type, Threshold, UnitOfMesure, M)), 'L', 'M', L, M, local(append(L, M, Supply))).

groceryList(Threshold, UnitOfMesure, Supply) :-
	distributedSolve(remote(fridgeGroceryList(Threshold, UnitOfMesure, L)), remote(pantryGroceryList(Threshold, UnitOfMesure, M)), 'L', 'M', L, M, local(append(L, M, Supply))).

groceryList(Threshold, Supply) :-
	distributedSolve(remote(fridgeGroceryList(Threshold, L)), remote(pantryGroceryList(Threshold, M)), 'L', 'M', L, M, local(append(L, M, Supply))).
	
suggestMixBasedOnGroceryNeeds(What) :-
	distributedSolve(remote(fridgeGroceryList(3, List)), remote(mixLiked(Liked)), 'List', 'Liked', List, Liked, local(suggest(List, Liked, What))).

suggestMixBasedOnGroceryNeeds(TolleranceTreshold, What) :-
	distributedSolve(remote(fridgeGroceryList(TolleranceTreshold, List)), remote(mixLiked(Liked)), 'List', 'Liked', List, Liked, local(suggest(List, Liked, What))).
	
suggest(List, Liked, What) :-
	processGrocery(List, [], R, ResList),
	analyzeMix(Liked, ResList), !,
	findall(enjoyThisMixAfterGrocery(L, K), enjoyThisMixAfterGrocery(L, K), What),
	retractDataList(What), !.
	
processGrocery([], R, W, ResList) :- 
	ResList = R.
processGrocery([groceryListItem(product(P,B))|T], Acc, R, ResList) :-
	append([P], Acc, R),
	processGrocery(T, R, W, ResList).
	
analyzeMix([], ResList).
analyzeMix([liked(L, K)|T], ResList) :-
	controlIngredients(L, L, K, ResList),
	analyzeMix(T, ResList).
analyzeMix([liked(L, K)|T], ResList) :-
	analyzeMix(T, ResList).
	
controlIngredients([], L, K, ResList) :-
	assert(enjoyThisMixAfterGrocery(L, K)).
controlIngredients([H|T], L, K, ResList) :-
	member(H, ResList),
	controlIngredients(T, L, K, ResList).
	
retractDataList([]).
retractDataList([H|T]) :- 
	retract(H), retractDataList(T).