package it.unibo.smartkitchen.agents.behaviours;

import java.util.Properties;

import alice.tuprolog.SolveInfo;
import alice.tuprologx.middleware.agents.interfaces.IAgentBehaviour;
import alice.tuprologx.middleware.agents.interfaces.IAgentCapabilities;
import it.unibo.smartkitchen.agents.SmartKitchenAgent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

//Alberto
public class DistributedSolveBehaviour extends Behaviour implements IAgentBehaviour {

	/**
     * @author Alberto Sita
     * 
     */
	
	private static final long serialVersionUID = 1L;
	
	private String messageType = null;
	private String content = null;
	private SmartKitchenAgent userAgent = null;
	private DFAgentDescription currentAgentId = null;
	private String toCall = null;
	private long timeout = 0;

	@Override
	public void setMessageType(String messageType){
		this.messageType = messageType;
	}
	
	@Override
	public void setAgent(IAgentCapabilities agent){
		this.userAgent = (SmartKitchenAgent) agent;
	}
	
	@Override
	public void setContent(String content){
		this.content = content;
	}
	
	@Override
	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
	
	@Override
	public void action() {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		Properties props = myAgent.getBootProperties();
	    String type = props.getProperty("agent-service-type");
     	sd.setType(type);
     	template.addServices(sd);
     	DFAgentDescription[] result = null;
     	try {
     		result = DFService.search(myAgent, template);
     	}
     	catch (FIPAException fe) {
     		fe.printStackTrace();
     		done();
     	}
     	toCall = null;
     	for(int i=0; i<result.length; i++){
     		toCall = userAgent.findLPaaS(result[i], content);
     		if(toCall != null){
     			currentAgentId = result[i];
     			break;
     		}
     	}
     	if(currentAgentId == null || toCall == null)
     		done();
		userAgent.sendMessageToLPaaS(currentAgentId, content, toCall, timeout, 0, messageType);
		done();
	}
	
	@Override
	public SolveInfo getSolveInfo(){
		return userAgent.getSolveInfo();
	}

	@Override
	public boolean done() {
		return true;
	}
	
}
