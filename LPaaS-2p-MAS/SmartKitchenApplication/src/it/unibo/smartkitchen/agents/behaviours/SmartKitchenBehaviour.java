package it.unibo.smartkitchen.agents.behaviours;

import java.awt.EventQueue;
import java.util.Properties;

import it.unibo.smartkitchen.agents.SmartKitchenAgent;
import it.unibo.smartkitchen.gui.SmartKitchenGUI;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

//Alberto
public class SmartKitchenBehaviour extends Behaviour {

	/**
     * @author Alberto Sita
     * 
     */
	
	private static final long serialVersionUID = 1L;
	
	private String messageType = null;
	private String content = null;
	private long maxTime = 0;
	private int numSol = 0;
	private SmartKitchenAgent userAgent = null;
	private SmartKitchenGUI GUI = null;
	private DFAgentDescription currentAgentId = null;
	private String toCall = null;
	
	public SmartKitchenBehaviour(SmartKitchenAgent userAgent, String content, long maxTime, int numSol, String messageType, SmartKitchenGUI userGUI){
		this.messageType = messageType;
		this.numSol = numSol;
		this.userAgent = userAgent;
		this.content = content;
		this.maxTime = maxTime;
		this.GUI = userGUI;
	}
	
	public SmartKitchenBehaviour(SmartKitchenAgent userAgent, String content, int numSol, String messageType, SmartKitchenGUI userGUI){
		this.messageType = messageType;
		this.userAgent = userAgent;
		this.numSol = numSol;
		this.content = content;
		this.GUI = userGUI;
	}
	
	public DFAgentDescription getAgentId(){
		return currentAgentId;
	}
	
	public String getComponentToCall(){
		return toCall;
	}

	@Override
	public void action() {
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		Properties props = myAgent.getBootProperties();
	    String type = props.getProperty("agent-service-type");
     	sd.setType(type);
     	template.addServices(sd);
     	DFAgentDescription[] result = null;
     	try {
     		result = DFService.search(myAgent, template);
     	}
     	catch (FIPAException fe) {
     		fe.printStackTrace();
     		done();
     	}
     	toCall = null;
     	for(int i=0; i<result.length; i++){
     		toCall = userAgent.findLPaaS(result[i], content);
     		if(toCall != null){
     			currentAgentId = result[i];
     			break;
     		}
     	}
		String res = userAgent.sendMessageToLPaaS(currentAgentId, content, toCall, maxTime, numSol, messageType);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI.refreshOutput(res+"\n");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		done();
	}

	@Override
	public boolean done() {
		return true;
	}

}
