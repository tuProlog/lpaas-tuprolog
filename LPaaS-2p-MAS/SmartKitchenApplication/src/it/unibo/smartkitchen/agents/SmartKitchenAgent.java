package it.unibo.smartkitchen.agents;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;

import alice.tuprolog.SolveInfo;
import alice.tuprologx.middleware.LPaaS.LPaaSRequest;
import alice.tuprologx.middleware.LPaaS.LPaaSResponse;
import alice.tuprologx.middleware.agents.interfaces.IAgentCapabilities;
import alice.tuprologx.middleware.agents.jade.java.ContainerAgent;
import it.unibo.smartkitchen.agents.behaviours.DistributedSolveBehaviour;
import it.unibo.smartkitchen.components.DistributedSolverComponent;
import it.unibo.smartkitchen.gui.SmartKitchenGUI;
import jade.core.AID;
import jade.core.ServiceException;
import jade.core.behaviours.TickerBehaviour;
import jade.core.security.SecurityHelper;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import jade.security.JADEPrincipal;
import jade.util.leap.Iterator;

//Alberto
public class SmartKitchenAgent extends ContainerAgent implements IAgentCapabilities {

	/**
     * @author Alberto Sita
     * 
     */
	
	private static final long serialVersionUID = 1L;
	
	private transient SecurityHelper helper = null;
	private transient SmartKitchenGUI GUI = null;
	private transient SolveInfo info = null;
	
	@Override
	public SolveInfo getSolveInfo(){
		return info;
	}

	@Override
    protected void setup() {
		super.setup();
		DFAgentDescription dfd = new DFAgentDescription();
	    dfd.setName(getAID());
	    ServiceDescription sd = new ServiceDescription();
	    String logicalName = "smart-Kitchen";
	    String type = "client";
	    sd.setType(type);
	    sd.setName(logicalName);
	    dfd.addServices(sd);
	    try {
	      DFService.register(this, dfd);
	    }
	    catch (FIPAException fe) {
	      fe.printStackTrace();
	      doDelete();
	    }
		configure();
	}
	
	private void configure() {
		SmartKitchenAgent smartKitchenAgent = this;
		DistributedSolverComponent distributedSolver = (DistributedSolverComponent) getComponent("distributedSolver");
		distributedSolver.setAgent(this);
		try {
			helper = (SecurityHelper) getHelper("jade.core.security.Security");
		} catch (ServiceException e) {
			e.printStackTrace();
			doDelete();
		}
	    GUI = new SmartKitchenGUI(smartKitchenAgent, distributedSolver);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		addBehaviour(new TickerBehaviour(this, 10000) {
			private static final long serialVersionUID = 1L;
			protected void onTick() {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							GUI.refreshConnectedAgents();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			} 
		});
	}

	@Override 
	protected void takeDown(){
		try{
			DFService.deregister(this);
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		} finally {
			super.takeDown();
		}
	}
	
	public DFAgentDescription[] discoverAgents(){
		DFAgentDescription template = new DFAgentDescription();
     	try {
     		DFAgentDescription[] result = DFService.search(this, template);
     		return result;
     	}
     	catch (FIPAException fe) {
     		fe.printStackTrace();
     		return null;
     	}
	}
	
	public String sendMessageToLPaaS(DFAgentDescription result, String content, String component, long maxTime, int numSol, String typeOfLPaaSRequest){
		SolveInfo info = null;
		this.info = null;
		if(result == null){
			if(maxTime>0)
				return typeOfLPaaSRequest+" unavailable for "+content+", timeout: "+maxTime;
			else
				return typeOfLPaaSRequest+" unavailable for "+content;
		}
		try {
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		    msg.addReceiver(result.getName());
		    LPaaSRequest lpaas_request = new LPaaSRequest();
		    lpaas_request.setComponentAction(typeOfLPaaSRequest);
		    if(maxTime>0)
		    	lpaas_request.setTimeout(maxTime);
		    if(component == null){
		    	System.out.println("Component Name in LPaaS request cannot be null!");
		    	return null;
		    }
		    lpaas_request.setComponentName(component);
		    lpaas_request.setContent(content);
		    if(numSol >0)
		    	lpaas_request.setNumSol(numSol);
		    try {
		    	msg.setContentObject(lpaas_request);
		    } catch (IOException e) {
		    	e.printStackTrace();
		    	return null;
		    }
		    msg.setConversationId("query_lpaas");
		    msg.setReplyWith("msg"+System.currentTimeMillis());
		    Iterator it = msg.getAllReceiver();
			while (it.hasNext()) {
				AID receiver = (AID) it.next();
				//if (helper.getTrustedPrincipal(receiver.getName()) == null) {
					try {
						JADEPrincipal principal = retrievePrincipal(receiver);
						helper.addTrustedPrincipal(principal);
					}
					catch (Exception e) {
						System.out.println(getName()+": Error retrieving the principal for agent "+receiver.getName());
						e.printStackTrace();
						return null;
					}
				//}
			}
			helper.setUseSignature(msg);
	        helper.setUseEncryption(msg);
	        send(msg);
		    msg = blockingReceive();
		    	if (msg != null) {
		    		if ("get-principal".equals(msg.getContent())) {
						ACLMessage reply = msg.createReply();
						reply.setPerformative(ACLMessage.INFORM);
						helper.setUseSignature(reply);
						send(reply);
						msg = blockingReceive();
		    		}
					LPaaSResponse resp = null;
					try {
						resp = (LPaaSResponse) msg.getContentObject();
					} catch (UnreadableException e) {
						e.printStackTrace();
					}
					info = resp.getSolveInfoIfAvailable();
					if(info != null){
						this.info = info;
						return info.toString();
					} else {
						ArrayList<SolveInfo> infos = resp.getArrayOfSolveInfoIfAvailable();
						if(infos != null){
							String r = infos.get(0).toString()+"\n";
							infos.remove(0);
							for(SolveInfo s : infos){
								r = r + s.toString()+ "\n";
							}
							return r;
						} else {
							return resp.getResponse();
						}
					}
				}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return null;
	}

	private JADEPrincipal retrievePrincipal(AID id) throws Exception {
		ACLMessage request = new ACLMessage(ACLMessage.REQUEST);
		request.addReceiver(id);
		request.setContent("get-principal");
		helper.setUseSignature(request);
		ACLMessage reply = FIPAService.doFipaRequestClient(this, request, 5000);
		if (reply != null) {
			return helper.getPrincipal(reply);
		}
		else {
			throw new Exception("No reply received to get-principal request for agent "+id.getName());
		}
	}

	public String findLPaaS(DFAgentDescription dfAgentDescription, String content) {
		SolveInfo info = null;
		try {
		    for(int i=1; ; i++){
		    	ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
		    	msg.addReceiver(dfAgentDescription.getName());
		    	LPaaSRequest lpaas_request = new LPaaSRequest();
		    	lpaas_request.setComponentAction(LPaaSRequest.IS_GOAL);
		    	String component = null;
		    	component = this.getBootProperties().getProperty("callable-component."+i);
		    	if(component == null)
		    		return null;
		    	lpaas_request.setComponentName(component);
		    	lpaas_request.setContent(content);
		    	try {
		    		msg.setContentObject(lpaas_request);
		    	} catch (IOException e) {
		    		e.printStackTrace();
		    		return null;
		    	}
		    	msg.setConversationId("query_lpaas");
		    	msg.setReplyWith("msg"+System.currentTimeMillis());
		    	Iterator it = msg.getAllReceiver();
		    	while (it.hasNext()) {
		    		AID receiver = (AID) it.next();
		    		//if (helper.getTrustedPrincipal(receiver.getName()) == null) {
		    			try {
		    				JADEPrincipal principal = retrievePrincipal(receiver);
		    				helper.addTrustedPrincipal(principal);
		    			}
		    			catch (Exception e) {
		    				System.out.println(getName()+": Error retrieving the principal for agent "+receiver.getName());
		    				e.printStackTrace();
		    				return null;
		    			}
		    		//}
		    	}
		    	helper.setUseSignature(msg);
		    	helper.setUseEncryption(msg);
		    	send(msg);
		    	msg = blockingReceive();
		    		if (msg != null) {
		    			if ("get-principal".equals(msg.getContent())) {
		    				ACLMessage reply = msg.createReply();
		    				reply.setPerformative(ACLMessage.INFORM);
		    				helper.setUseSignature(reply);
		    				send(reply);
		    				msg = blockingReceive();
		    			}
		    			LPaaSResponse resp = null;
		    			try {
		    				resp = (LPaaSResponse) msg.getContentObject();
		    			} catch (UnreadableException e) {
		    				e.printStackTrace();
		    				return null;
		    			}
		    			info = resp.getSolveInfoIfAvailable();
		    			String result = null;
		    			if(info != null){
		    				result = info.toString();
		    			} else {
		    				result = resp.getResponse();
		    			}
		    			if(result != null && result.equals("yes.")){
		    				return component;
		    			}
		    		}
		    }
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void addBehaviour(Object behaviour) {
		addBehaviour((DistributedSolveBehaviour) behaviour);
	}
	
	@Override
    protected void beforeMove() {
		super.beforeMove();
		if(GUI!=null){
			GUI.setVisible(false);
			GUI.dispose();
		}
    }
	
    @Override
    protected void afterMove() {
        super.afterMove();
        configure();
    }

}
