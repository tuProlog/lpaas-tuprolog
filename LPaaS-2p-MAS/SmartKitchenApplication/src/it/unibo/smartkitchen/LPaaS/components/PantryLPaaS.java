package it.unibo.smartkitchen.LPaaS.components;

//import java.awt.EventQueue;
//import javax.swing.JFrame;

import alice.tuprolog.Prolog;
//import alice.tuprolog.management.PrologMXBeanServer;
import alice.tuprologx.middleware.LPaaS.LPaaSComponent;
import alice.tuprologx.middleware.LPaaS.annotations.GoalsToMatch;
import alice.tuprologx.middleware.annotations.AsSingleton;
//import alice.tuprologx.middleware.annotations.OnDispose;
//import alice.tuprologx.middleware.annotations.PostConstructCall;
//import alice.tuprologx.middleware.annotations.PostMigrationSetup;
//import alice.tuprologx.middleware.annotations.PreMigrationSetup;
import alice.tuprologx.middleware.annotations.PrologConfiguration;
//import alice.tuprologx.middleware.annotations.PrologManagement;
//import it.unibo.smartkitchen.LPaaS.components.gui.LPaaSComponentGUI;

//Alberto
@AsSingleton
public class PantryLPaaS extends LPaaSComponent {

	/**
     * @author Alberto Sita
     * 
     */
	
	//Codice commentato perchè questo componente gira sul raspberry
	//Dichiarata solamente la risorsa prolog
	
	/*@PrologManagement(host = "localhost", port = 45004, lazyBoot = false,
			adaptor = PrologMXBeanServer.HTTP_ADAPTOR,
			credentialFile = "./credential.txt",
			SSLconfigFile = "./ssl.properties")*/
	@PrologConfiguration(directives = {"set_prolog_flag(occursCheck, off)."},
			fromFiles = {"pantryTheory.pl"})
	@GoalsToMatch(toMatch = {"pantryData(List).", "pantryModelInfo(Info).",
			"pantryGroceryList(Type, Threshold, UnitOfMesure, Supply).",
			"pantryGroceryList(Threshold, UnitOfMesure, Supply).",
			"pantryGroceryList(Threshold, Supply).", "pantryStatus(S).",
			"getPantryTemperature(Temperature, UnitOfMeasure).", "pantryAeringSystemStatus(Status)."})
	private Prolog prolog;
	
	//private JFrame frame = null;
	
	/*@PostConstructCall
	  @PostMigrationSetup
	public void setupGUI(){
		LPaaSComponent component = this;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new LPaaSComponentGUI("Pantry", component);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@OnDispose
	@PreMigrationSetup
	public void disposeGUI(){
		if(frame!=null){
			frame.setVisible(false);
			frame.dispose();
		}
	}*/

}
