package it.unibo.smartkitchen.LPaaS.components.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.google.gson.Gson;

import alice.tuprolog.SolveInfo;
import alice.tuprologx.middleware.LPaaS.LPaaSComponent;
import alice.tuprologx.middleware.LPaaS.LPaaSRequest;
import it.unibo.smartkitchen.gui.NumberImputGUI;
import it.unibo.smartkitchen.gui.interfaces.GeneralGUI;

//Alberto
public class LPaaSComponentGUI extends JFrame implements ActionListener, GeneralGUI {
	
	/**
     * @author Alberto Sita
     * 
     */

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane = null;
	private JButton solve = null;
	private JButton solveTimed = null;
	private JButton solveN = null;
	private JButton solveN_timed = null;
	private JButton solveAll = null;
	private JButton solveAll_timed = null;
	private JButton addGoal = null;
	private JButton removeGoal = null;
	private JButton addTheory = null;
	private JButton clearGUI = null;
	private TextArea inputArea = null;
	private TextArea outputArea = null;
	private JLabel label1 = null;
	private JLabel label2 = null;
	
	private LPaaSComponent component = null;
	
	private long timer = 0;
	private int numberOfSol = 0;
	
	public LPaaSComponentGUI(String name, LPaaSComponent component) {
		super(name+" GUI");
		this.component = component;
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		label1 = new JLabel("Input");
		label2 = new JLabel("Output");
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BorderLayout(0,0));
		inputArea = new TextArea();
		inputPanel.add(inputArea, BorderLayout.CENTER);
		JPanel buttonsPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(0,5);
		buttonsPanel.setLayout(gridLayout);
		solve = new JButton("Solve");
		solve.addActionListener(this);
		solveTimed = new JButton("Solve timed");
		solveTimed.addActionListener(this);
		solveN = new JButton("Solve N");
		solveN.addActionListener(this);
		solveN_timed = new JButton("Solve N timed");
		solveN_timed.addActionListener(this);
		solveAll = new JButton("Solve all");
		solveAll.addActionListener(this);
		solveAll_timed = new JButton("Solve all timed");
		solveAll_timed.addActionListener(this);
		addGoal = new JButton("Add goal");
		addGoal.addActionListener(this);
		removeGoal = new JButton("Remove goal");
		removeGoal.addActionListener(this);
		addTheory = new JButton("Add theory");
		addTheory.addActionListener(this);
		clearGUI = new JButton("Clear GUI");
		clearGUI.addActionListener(this);
		buttonsPanel.add(solve);
		buttonsPanel.add(solveTimed);
		buttonsPanel.add(solveN);
		buttonsPanel.add(solveN_timed);
		buttonsPanel.add(solveAll);
		buttonsPanel.add(solveAll_timed);
		buttonsPanel.add(addGoal);
		buttonsPanel.add(removeGoal);
		buttonsPanel.add(addTheory);
		buttonsPanel.add(clearGUI);
		JPanel outputPanel = new JPanel();
		outputPanel.setLayout(new BorderLayout(0,0));
		outputArea = new TextArea();
		outputArea.setEditable(false);
		outputPanel.add(outputArea, BorderLayout.CENTER);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		JPanel label1Panel = new JPanel();
		label1Panel.add(label1);
		contentPane.add(label1Panel);
		contentPane.add(inputPanel);
		contentPane.add(buttonsPanel);
		JPanel label2Panel = new JPanel();
		label2Panel.add(label2);
		contentPane.add(label2Panel);
		contentPane.add(outputPanel);
		setContentPane(contentPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase("Solve")){
			performSolveAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Solve timed")){
			performSolveTimedAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve N")){
			performSolveNAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve N timed")){
			performSolveNTimedAction(false, 0);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve all")){
			performSolveAllAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Solve all timed")){
			performSolveAllTimedAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Add goal")){
			performAddGoalAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Remove goal")){
			performRemoveGoalAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Add theory")){
			performAddTheoryAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Clear GUI")){
			performClearGUIAction();
		} else 
			return;
	}

	@Override
	public void performSolveAllTimedAction(boolean skip) {
		outputArea.setText("");
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			String res = component.solveAll(input, timer);
			SolveInfo info = null;
			try{
				Gson g = new Gson();
				@SuppressWarnings("unchecked")
				ArrayList<String> r = g.fromJson(res, ArrayList.class);
				for(String s: r){
					info = SolveInfo.fromJSON(s);
					if(outputArea.getText().isEmpty())
						outputArea.setText(info.toString());
					else
						outputArea.setText(outputArea.getText()+"\n"+info.toString());
				}
				outputArea.setText(outputArea.getText()+"\n");
			} catch (Exception e) {
				outputArea.setText(res);
			}
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_ALL_TIMED, "Timer?");
			timing.setVisible(true);
		}
	}

	@Override
	public void performSolveNTimedAction(boolean skip, int gui) {
		outputArea.setText("");
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			String res = component.solveN(input, numberOfSol, timer);
			SolveInfo info = null;
			try{
				Gson g = new Gson();
				@SuppressWarnings("unchecked")
				ArrayList<String> r = g.fromJson(res, ArrayList.class);
				for(String s: r){
					info = SolveInfo.fromJSON(s);
					if(outputArea.getText().isEmpty())
						outputArea.setText(info.toString());
					else
						outputArea.setText(outputArea.getText()+"\n"+info.toString());
				}
				outputArea.setText(outputArea.getText()+"\n");
			} catch (Exception e) {
				outputArea.setText(res);
			}
		} else {
			if(gui == 0){
				NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N_TIMED, "Number of solution?");
				timing.setVisible(true);
			} else if (gui == 1) {
				NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N_TIMED, "Timer?");
				timing.setVisible(true);
			}
		}
	}

	private synchronized void performSolveAllAction() {
		outputArea.setText("");
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		String res = component.solveAll(input);
		SolveInfo info = null;
		try{
			Gson g = new Gson();
			@SuppressWarnings("unchecked")
			ArrayList<String> r = g.fromJson(res, ArrayList.class);
			for(String s: r){
				info = SolveInfo.fromJSON(s);
				if(outputArea.getText().isEmpty())
					outputArea.setText(info.toString());
				else
					outputArea.setText(outputArea.getText()+"\n"+info.toString());
			}
			outputArea.setText(outputArea.getText()+"\n");
		} catch (Exception e) {
			outputArea.setText(res);
		}
	}

	private synchronized void performAddTheoryAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		String res = component.addTheory(input);
		outputArea.setText(res);
	}

	private void performClearGUIAction() {
		inputArea.getText();
		inputArea.setText("");
		outputArea.setText("");
	}

	private synchronized void performRemoveGoalAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		String res = component.removeGoal(input);
		outputArea.setText(res);
	}

	private synchronized void performAddGoalAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		String res = component.addGoal(input);
		outputArea.setText(res);
	}

	@Override
	public synchronized void performSolveNAction(boolean skip) {
		outputArea.setText("");
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			String res = component.solveN(input, numberOfSol);
			SolveInfo info = null;
			try{
				Gson g = new Gson();
				@SuppressWarnings("unchecked")
				ArrayList<String> r = g.fromJson(res, ArrayList.class);
				for(String s: r){
					info = SolveInfo.fromJSON(s);
					if(outputArea.getText().isEmpty())
						outputArea.setText(info.toString());
					else
						outputArea.setText(outputArea.getText()+"\n"+info.toString());
				}
				outputArea.setText(outputArea.getText()+"\n");
			} catch (Exception e) {
				outputArea.setText(res);
			}
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N, "Number of solution?");
			timing.setVisible(true);
		}
	}

	@Override
	public synchronized void performSolveTimedAction(boolean skip) {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			String res = component.solve(input, timer);
			SolveInfo info = null;
			try{
				info = SolveInfo.fromJSON(res);
			} catch (Exception e) {
				outputArea.setText(res);
			}
			if(info != null)
				outputArea.setText(info.toString()+"\n");
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_TIMED, "Timer?");
			timing.setVisible(true);
		}
	}

	private synchronized void performSolveAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		String res = component.solve(input);
		SolveInfo info = null;
		try{
			info = SolveInfo.fromJSON(res);
		} catch (Exception e) {
			outputArea.setText(res);
		}
		if(info != null)
			outputArea.setText(info.toString()+"\n");
	}

	@Override
	public void setTimer(long timer) {
		this.timer = timer;
	}

	@Override
	public void setNumberOfSol(int numberOfSol) {
		this.numberOfSol = numberOfSol;
	}

}
