package it.unibo.smartkitchen.LPaaS.components;

import java.awt.EventQueue;
import javax.swing.JFrame;

import alice.tuprolog.Prolog;
import alice.tuprolog.management.PrologMXBeanServer;
import alice.tuprologx.middleware.LPaaS.LPaaSComponent;
import alice.tuprologx.middleware.LPaaS.annotations.GoalsToMatch;
import alice.tuprologx.middleware.annotations.AsSingleton;
import alice.tuprologx.middleware.annotations.OnDispose;
import alice.tuprologx.middleware.annotations.PostConstructCall;
import alice.tuprologx.middleware.annotations.PostMigrationSetup;
import alice.tuprologx.middleware.annotations.PreMigrationSetup;
import alice.tuprologx.middleware.annotations.PrologConfiguration;
import alice.tuprologx.middleware.annotations.PrologManagement;
import it.unibo.smartkitchen.LPaaS.components.gui.LPaaSComponentGUI;

//Alberto
@AsSingleton
public class FridgeLPaaS extends LPaaSComponent {

	/**
     * @author Alberto Sita
     * 
     */
	
	@PrologManagement(host = "localhost", port = 45001, lazyBoot = false,
			adaptor = PrologMXBeanServer.HTTP_ADAPTOR,
			credentialFile = "./credential.txt",
			SSLconfigFile = "./ssl.properties")
	@PrologConfiguration(directives = {"set_prolog_flag(occursCheck, off)."},
			fromFiles = {"fridgeTheory.pl"})
	@GoalsToMatch(toMatch = {"fridgeData(List).", "fridgeModelInfo(Info).",
			"fridgeGroceryList(Type, Threshold, UnitOfMesure, Supply).",
			"fridgeGroceryList(Threshold, UnitOfMesure, Supply).",
			"fridgeGroceryList(Threshold, Supply).",
			"getFridgeTemperature(Temperature, UnitOfMeasure).", "fridgeAntiIceSystemStatus(Status).",
			"fridgeCoolingSystemStatus(Status).", "fridgeStatus(Status)."})
	private Prolog prolog;
	
	private JFrame frame = null;
	
	@PostConstructCall
	@PostMigrationSetup
	public void setupGUI(){
		LPaaSComponent component = this;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new LPaaSComponentGUI("Fridge", component);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@OnDispose
	@PreMigrationSetup
	public void disposeGUI(){
		if(frame!=null){
			frame.setVisible(false);
			frame.dispose();
		}
	}

}
