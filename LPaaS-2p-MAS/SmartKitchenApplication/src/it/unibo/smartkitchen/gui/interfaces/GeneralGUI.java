package it.unibo.smartkitchen.gui.interfaces;

//Alberto
public interface GeneralGUI {
	
	/**
     * @author Alberto Sita
     * 
     */
	
	void performSolveTimedAction(boolean skip);
	void performSolveNAction(boolean skip);
	void performSolveAllTimedAction(boolean skip);
	void performSolveNTimedAction(boolean skip, int gui);
	void setTimer(long timer);
	void setNumberOfSol(int numberOfSol);

}
