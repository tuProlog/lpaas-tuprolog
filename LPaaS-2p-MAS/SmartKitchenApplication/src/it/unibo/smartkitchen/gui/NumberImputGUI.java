package it.unibo.smartkitchen.gui;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import alice.tuprologx.middleware.LPaaS.LPaaSRequest;
import it.unibo.smartkitchen.gui.interfaces.GeneralGUI;

//Alberto
public class NumberImputGUI extends JFrame implements ActionListener {
	
	/**
     * @author Alberto Sita
     * 
     */

	private static final long serialVersionUID = 1L;
	private JPanel contentPane = null;
	private TextArea area = null;
	private JButton button = null;
	private String typeAction = null;
	private GeneralGUI userGUI = null;

	public NumberImputGUI(GeneralGUI userGUI, String typeAction, String title) {
		super(title);
		this.typeAction = typeAction;
		this.userGUI = userGUI;
		setBounds(100, 100, 250, 100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		area = new TextArea();
		if(typeAction.equals(LPaaSRequest.SOLVE_TIMED) || typeAction.equals(LPaaSRequest.SOLVE_ALL_TIMED))
			button = new JButton("Set Timer");
		else if(typeAction.equals(LPaaSRequest.SOLVE_N))
			button = new JButton("Set Number of sol.");
		if(typeAction.equals(LPaaSRequest.SOLVE_N_TIMED)){
			if(title.equals("Timer?")){
				button = new JButton("Set Timer");
			} else {
				button = new JButton("Set Number of sol.");
			}
		}
		button.addActionListener(this);
		contentPane.add(area, BorderLayout.CENTER);
		contentPane.add(button, BorderLayout.PAGE_END);
		setContentPane(contentPane);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String time = area.getText().trim();
		try{
			if(typeAction.equals(LPaaSRequest.SOLVE_TIMED)){
				userGUI.setTimer(Long.parseLong(time));
				this.setVisible(false);
				userGUI.performSolveTimedAction(true);
			} else if(typeAction.equals(LPaaSRequest.SOLVE_N)){
				userGUI.setNumberOfSol(Integer.parseInt(time));
				this.setVisible(false);
				userGUI.performSolveNAction(true);
			} else if(typeAction.equals(LPaaSRequest.SOLVE_ALL_TIMED)){
				userGUI.setTimer(Long.parseLong(time));
				this.setVisible(false);
				userGUI.performSolveAllTimedAction(true);
			} else if(typeAction.equals(LPaaSRequest.SOLVE_N_TIMED)){
				if(e.getActionCommand().equals("Set Timer")){
					userGUI.setTimer(Long.parseLong(time));
					this.setVisible(false);
					userGUI.performSolveNTimedAction(true, -1);
				} else {
					userGUI.setNumberOfSol(Integer.parseInt(time));
					this.setVisible(false);
					userGUI.performSolveNTimedAction(false, 1);
				}
			} else return;
		} catch (Exception ex){
			ex.printStackTrace();
			return;
		}
	}
	
}
