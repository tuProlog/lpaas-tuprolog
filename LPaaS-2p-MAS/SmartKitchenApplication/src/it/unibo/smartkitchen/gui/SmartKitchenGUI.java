package it.unibo.smartkitchen.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import alice.tuprolog.SolveInfo;
import alice.tuprologx.middleware.LPaaS.LPaaSRequest;
import it.unibo.smartkitchen.agents.SmartKitchenAgent;
import it.unibo.smartkitchen.agents.behaviours.SmartKitchenBehaviour;
import it.unibo.smartkitchen.components.DistributedSolverComponent;
import it.unibo.smartkitchen.gui.interfaces.GeneralGUI;
import jade.domain.FIPAAgentManagement.DFAgentDescription;

//Alberto
public class SmartKitchenGUI extends JFrame implements ActionListener, GeneralGUI {
	
	/**
     * @author Alberto Sita
     * 
     */

	private static final long serialVersionUID = 1L;
	
	private SmartKitchenAgent myAgent = null;
	
	private JPanel contentPane = null;
	private JButton solve = null;
	private JButton dsolve = null;
	private JButton solveTimed = null;
	private JButton solveN = null;
	private JButton solveN_timed = null;
	private JButton solveAll = null;
	private JButton solveAll_timed = null;
	private JButton clearGUI = null;
	private TextArea inputArea = null;
	private TextArea outputArea = null;
	private TextArea connectedAgentsArea = null;
	private JLabel label1 = null;
	private JLabel label2 = null;
	private JLabel label3 = null;
	
	private long timer = 0;
	private int numberOfSol = 0;

	private DistributedSolverComponent distributedSolver = null;
	
	public SmartKitchenGUI(SmartKitchenAgent userAgent, DistributedSolverComponent distributedSolver) {
		super("Smart Kitchen GUI");
		myAgent = userAgent;
		this.distributedSolver = distributedSolver;
		setBounds(100, 100, 700, 750);
		contentPane = new JPanel();
		label1 = new JLabel("Input");
		label2 = new JLabel("Output");
		label3 = new JLabel("Connected Agents");
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new BorderLayout(0,0));
		inputArea = new TextArea();
		inputPanel.add(inputArea, BorderLayout.CENTER);
		JPanel buttonsPanel = new JPanel();
		GridLayout gridLayout = new GridLayout(0,4);
		buttonsPanel.setLayout(gridLayout);
		solve = new JButton("Solve");
		solve.addActionListener(this);
		dsolve = new JButton("Distributed solve");
		dsolve.addActionListener(this);
		solveTimed = new JButton("Solve timed");
		solveTimed.addActionListener(this);
		solveN = new JButton("Solve N");
		solveN.addActionListener(this);
		solveN_timed = new JButton("Solve N timed");
		solveN_timed.addActionListener(this);
		solveAll = new JButton("Solve all");
		solveAll.addActionListener(this);
		solveAll_timed = new JButton("Solve all timed");
		solveAll_timed.addActionListener(this);
		clearGUI =  new JButton("Clear GUI");
		clearGUI.addActionListener(this);
		buttonsPanel.add(solve);
		buttonsPanel.add(solveTimed);
		buttonsPanel.add(solveN);
		buttonsPanel.add(solveN_timed);
		buttonsPanel.add(solveAll);
		buttonsPanel.add(solveAll_timed);
		//buttonsPanel.add(dsolve);
		buttonsPanel.add(clearGUI);
		JPanel outputPanel = new JPanel();
		outputPanel.setLayout(new BorderLayout(0,0));
		outputArea = new TextArea();
		outputArea.setEditable(false);
		outputPanel.add(outputArea, BorderLayout.CENTER);
		JPanel agentsPanel = new JPanel();
		agentsPanel.setLayout(new BorderLayout(0,0));
		connectedAgentsArea = new TextArea();
		connectedAgentsArea.setEditable(false);
		DFAgentDescription[] agentsdescr = myAgent.discoverAgents();
		configureTextArea(agentsdescr, connectedAgentsArea);
		agentsPanel.add(connectedAgentsArea, BorderLayout.CENTER);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.PAGE_AXIS));
		JPanel label1Panel = new JPanel();
		label1Panel.add(label1);
		contentPane.add(label1Panel);
		contentPane.add(inputPanel);
		contentPane.add(buttonsPanel);
		JPanel label2Panel = new JPanel();
		label2Panel.add(label2);
		contentPane.add(label2Panel);
		contentPane.add(outputPanel);
		JPanel label3Panel = new JPanel();
		label3Panel.add(label3);
		contentPane.add(label3Panel);
		contentPane.add(agentsPanel);
		setContentPane(contentPane);
	}
	
	public void refreshConnectedAgents(){
		configureTextArea(myAgent.discoverAgents(), connectedAgentsArea);
	}

	private void configureTextArea(DFAgentDescription[] agentsdescr, TextArea connectedAgentsArea) {
		String toDisplay = "";
		if(agentsdescr!=null){
			for(int i=0; i<agentsdescr.length; i++){
     			toDisplay = toDisplay + agentsdescr[i].getName() + "\n";
     		}
		} else {
			toDisplay = "Internal error, cannot retrieve agents informations.";
		}
		connectedAgentsArea.setText(toDisplay);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equalsIgnoreCase("Solve")){
			performSolveAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Solve timed")){
			performSolveTimedAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve N")){
			performSolveNAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve N timed")){
			performSolveNTimedAction(false, 0);
		} else if(e.getActionCommand().equalsIgnoreCase("Solve all")){
			performSolveAllAction();
		} else if(e.getActionCommand().equalsIgnoreCase("Solve all timed")){
			performSolveAllTimedAction(false);
		} else if(e.getActionCommand().equalsIgnoreCase("Clear GUI")){
			performClearGUIAction();
		}else if(e.getActionCommand().equalsIgnoreCase("Distributed solve")){
			performDistributedSolveAction();
		}else 
			return;
	}

	@Override
	public void performSolveAllTimedAction(boolean skip) {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, timer, 0, LPaaSRequest.SOLVE_ALL_TIMED, this);
			myAgent.addBehaviour(b);
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_ALL_TIMED, "Timer?");
			timing.setVisible(true);
		}
	}

	@Override
	public void performSolveNTimedAction(boolean skip, int gui) {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, timer, numberOfSol, LPaaSRequest.SOLVE_N_TIMED, this);
			myAgent.addBehaviour(b);
		} else {
			if (gui == 0) {
				NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N_TIMED, "Number of solution?");
				timing.setVisible(true);
			} else if (gui == 1) {
				NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N_TIMED, "Timer?");
				timing.setVisible(true);
			}
		}
	}

	private void performSolveAllAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, 0, LPaaSRequest.SOLVE_ALL, this);
		myAgent.addBehaviour(b);
	}

	@Override
	public void performSolveNAction(boolean skip) {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, numberOfSol, LPaaSRequest.SOLVE_N, this);
			myAgent.addBehaviour(b);
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_N, "Number of solution?");
			timing.setVisible(true);
		}
	}

	private void performDistributedSolveAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		try {
			SolveInfo info = distributedSolver.distributedSolve(input);
			outputArea.setText(info.toString()+"\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void performClearGUIAction() {
		inputArea.getText();
		inputArea.setText("");
		outputArea.setText("");
	}

	@Override
	public void performSolveTimedAction(boolean skip) {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		if(skip){
			SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, timer, 0, LPaaSRequest.SOLVE_TIMED, this);
			myAgent.addBehaviour(b);
		} else {
			NumberImputGUI timing = new NumberImputGUI(this, LPaaSRequest.SOLVE_TIMED, "Timer?");
			timing.setVisible(true);
		}
	}

	private void performSolveAction() {
		String input = inputArea.getText().trim();
		if(input.isEmpty())
			return;
		SmartKitchenBehaviour b = new SmartKitchenBehaviour(myAgent, input, 0, LPaaSRequest.SOLVE, this);
		myAgent.addBehaviour(b);
	}

	public void refreshOutput(String res) {
		outputArea.setText(res);
	}

	@Override
	public void setTimer(long timer) {
		this.timer = timer;
	}
	
	@Override
	public void setNumberOfSol(int numberOfSol) {
		this.numberOfSol = numberOfSol;
	}

}
