package it.unibo.smartkitchen.components;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.management.PrologMXBeanServer;
import alice.tuprologx.middleware.agents.interfaces.IAgentCapabilities;
import alice.tuprologx.middleware.annotations.AsSingleton;
import alice.tuprologx.middleware.annotations.PrologConfiguration;
import alice.tuprologx.middleware.annotations.PrologManagement;
import alice.tuprologx.middleware.lib.DistributedSolveLibrary;

//Alberto
@AsSingleton
public class DistributedSolverComponent {
	
	/**
     * @author Alberto Sita
     * 
     */
	
	@PrologManagement(host = "localhost", port = 45000, lazyBoot = false,
			adaptor = PrologMXBeanServer.HTTP_ADAPTOR,
			credentialFile ="./credential.txt",
			SSLconfigFile = "./ssl.properties")
	@PrologConfiguration(directives = {"set_prolog_flag(occursCheck, on).", 
			"load_library('alice.tuprologx.middleware.lib.DistributedSolveLibrary')."},
			fromFiles = {"distributedSolverComponentTheory.pl"})
	private Prolog prolog;
	
	private boolean configured = false;
	private IAgentCapabilities agentCapabilities = null;
	
	public void setAgent(IAgentCapabilities agentCapabilities){
		this.agentCapabilities = agentCapabilities;
		DistributedSolveLibrary lib = (DistributedSolveLibrary)prolog.getLibrary("alice.tuprologx.middleware.lib.DistributedSolveLibrary");
		lib.setAgentSupport(this.agentCapabilities);
		configured = true;
	}

	public boolean isCorrectlyConfigured(){
		return configured;
	}
	
	public SolveInfo distributedSolve(String query) throws Exception {
		if(isCorrectlyConfigured())
			return prolog.solve(query);
		else throw new Exception("DistributedSolverComponent not configured!");
	}

}
