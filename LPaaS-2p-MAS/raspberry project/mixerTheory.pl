mixLiked(Liked) :-
	findall(liked(X, Y), liked(X, Y), Li),
	retractDataList(Li),
	findall(mixerData(D), mixerData(D), ListData),
	findLikedMixes(ListData),
	findall(liked(X, Y), liked(X, Y), Liked),
	retractDataList(Liked), !.
	
findLikedMixes([]).
findLikedMixes([mixerData(L)|T]) :-
	checkMix(L),
	findLikedMixes(T).
	
checkMix([]).
checkMix([mixedElements(V, liked(yes), K, timestamp(M))|T]) :-
	getTime(B),
	B==M,
	assert(liked(V, K)),
	checkMix(T).
checkMix([mixedElements(V, liked(yes), K, timestamp(M))|T]) :-
	checkMix(T).
checkMix([mixedElements(V, liked(no), _, timestamp(M))|T]) :-
	checkMix(T).	

mixerModelInfo(Result) :-
	mixerModel(A, B),
	Result = mixerModel(A, B).
	
mixerStatus(S) :-
	getTime(O),
	mixerStatus(A, timestamp(O)),
	S=A,
	findall(mixerStatus(A, B), mixerStatus(A, B), L),
	retractDataList(L),
	assert(mixerStatus(A, timestamp(O))), !.

retractDataList([]).
retractDataList([H|T]) :- 
	retract(H), retractDataList(T).

naiverev([],[]). 
naiverev([H|T],R) :-
	naiverev(T, RevT),  
	append(RevT,[H],R).
	
getTime(M) :-
	findall(mixerLogicTime(T), mixerLogicTime(T), L),
	naiverev(L, [mixerLogicTime(U)|T]),
	M = U,
	retractDataList(L),
	assert(mixerLogicTime(M)), !.

%tickTime :-
%	getTime(M),
%	N is M+1,
%	assert(mixerLogicTime(N)), !.
	
%simulated Data -----

mixerLogicTime(0).

mixerData([mixedElements([mela, pera, banana], liked(yes), kcal(89), timestamp(0)),
	mixedElements([carota, arancia, pompelmo], liked(no), kcal(78), timestamp(0))]).	
mixerData([mixedElements([kiwi, pesca, pompelmo], liked(yes), kcal(80), timestamp(0))]).
mixerData([mixedElements([arancia, pesca], liked(yes), kcal(40), timestamp(0))]).
	
mixerModel(lg, see786bjn).

mixerStatus(off, timestamp(0)).