pantryGroceryList(Type, Threshold, UnitOfMesure, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(pantryData(D), pantryData(D), ListData),
	analyzePantryDataList(Type, Threshold, UnitOfMesure, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
pantryGroceryList(Threshold, UnitOfMesure, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(pantryData(D), pantryData(D), ListData),
	analyzePantryDataList(Threshold, UnitOfMesure, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
pantryGroceryList(Threshold, Supply) :-
	findall(groceryListItem(X), groceryListItem(X), S),
	retractDataList(S),
	findall(pantryData(D), pantryData(D), ListData),
	analyzePantryDataList(Threshold, ListData),
	findall(groceryListItem(X), groceryListItem(X), Supply),
	retractDataList(Supply), !.
	
analyzePantryDataList(Type, Threshold, UnitOfMesure, []).
analyzePantryDataList(Type, Threshold, UnitOfMesure, [pantryData(L)|T]) :-
	processEntry(Type, Threshold, UnitOfMesure, L), 
	analyzePantryDataList(Type, Threshold, UnitOfMesure, T).
	
analyzePantryDataList(Threshold, UnitOfMesure, []).
analyzePantryDataList(Threshold, UnitOfMesure, [pantryData(L)|T]) :-
	processEntry(Threshold, UnitOfMesure, L), 
	analyzePantryDataList(Threshold, UnitOfMesure, T).
	
analyzePantryDataList(Threshold, []).
analyzePantryDataList(Threshold, [pantryData(L)|T]) :-
	processEntry(Threshold, L), 
	analyzePantryDataList(Threshold, T).
	
processEntry(Type, Threshold, UnitOfMesure, []).
processEntry(Type, Threshold, UnitOfMesure, [pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Type, Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Type, Threshold, UnitOfMesure, T).
	
processEntry(Threshold, UnitOfMesure, []).
processEntry(Threshold, UnitOfMesure, [pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Threshold, UnitOfMesure, T).

processEntry(Threshold, []).
processEntry(Threshold, [pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))|T]) :- 
	verify(Threshold, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))), 
	processEntry(Threshold, T).

verify(Type, Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	Type == ItemType,
	UnitOfMesure == ItemUnitOfMesure,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Type, Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

verify(Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	UnitOfMesure == ItemUnitOfMesure,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Threshold, UnitOfMesure, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

verify(Threshold, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))) :-
	getTime(B),
	B==M,
	Quantity < Threshold,
	assert(groceryListItem(product(Item, Brand))).
verify(Threshold, pantrySupply(Item, ItemType, Brand, Quantity, ItemUnitOfMesure, timestamp(M))).

pantryModelInfo(Result) :-
	pantryModel(A, B),
	Result = pantryModel(A, B).
	
getPantryTemperature(T, S) :-
	getTime(O),
	pantryTemperature(A, B, timestamp(O)),
	T=A,
	S=B,
	findall(pantryTemperature(A, B, C), pantryTemperature(A, B, C), L),
	retractDataList(L),
	assert(pantryTemperature(A, B, timestamp(O))), !.
	
pantryAeringSystemStatus(S) :-
	getTime(O),
	pantryAering(A, timestamp(O)),
	S=A,
	findall(pantryAering(A, B), pantryAering(A, B), L),
	retractDataList(L),
	assert(pantryAering(A, timestamp(O))), !.
	
pantryStatus(S) :-
	getTime(O),
	pantryStatus(A, timestamp(I)),
	S=A,
	findall(pantryStatus(A, B), pantryStatus(A, B), L),
	retractDataList(L),
	assert(pantryStatus(A, timestamp(O))), !.

retractDataList([]).
retractDataList([H|T]) :- 
	retract(H), retractDataList(T).
	
naiverev([],[]). 
naiverev([H|T],R) :-
	naiverev(T, RevT),  
	append(RevT,[H],R).
	
getTime(M) :-
	findall(pantryLogicTime(T), pantryLogicTime(T), L),
	naiverev(L, [pantryLogicTime(U)|T]),
	M = U,
	retractDataList(L),
	assert(pantryLogicTime(M)), !.

%tickTime :-
%	getTime(M),
%	N is M+1,
%	assert(pantryLogicTime(N)), !.
	
%simulated Data -----

pantryLogicTime(0).

pantryData([pantrySupply(choco_pops, cereali, 'brand-o', 2, scatola, timestamp(0)),
	pantrySupply(passata, sugo, 'brand-u', 3, barattolo, timestamp(0)),
	pantrySupply(macine, biscotti, 'brand-x', 1, scatola, timestamp(0)),
	pantrySupply(saccottino, brioche, 'brand-x', 2, scatola, timestamp(0))]).
	
pantryData([pantrySupply(crackers, snack, 'brand-z', 2, scatola, timestamp(0)),
	pantrySupply(schiacciatina, snack, 'brand-y', 3, scatola, timestamp(0))]).

pantryModel(scavolini, bnmghj89l0).

pantryTemperature(25, gradi_celsius, timestamp(0)).

pantryAering(on, timestamp(0)).

pantryStatus(on, timestamp(0)).
			
